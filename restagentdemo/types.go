package restagentdemo

import (
	"errors"
)

type Request struct {
	ID_votant AgentID       `json:"id_votant"`
	Prefs     []Alternative `json:"Prefs"`
}

type Response struct {
	Result int `json:"res"`
}

type Request2 struct {
	Method string        `json:"met"`
	ID     AgentID       `json:"id"`
	Prefs  []Alternative `json:"args"`
}

type Response2 struct {
	Result string `json:"res"`
}

//Préférence
type Alternative int

//Liste des votants avec leurs préférences
type Profile [][]Alternative

//Pour chaque préférence associe son nombre de votes
type Count map[Alternative]int

//Crée un agent
type AgentID int

type Agent struct {
	ID    AgentID
	Name  string
	Prefs []Alternative
}

func (ag1 Agent) Equal(ag2 Agent) bool {
	if ag1.ID != ag2.ID {
		return false
	}
	// Pas obligatoire à partir de là, à discuter...
	if ag1.Name != ag2.Name {
		return false
	}

	if len(ag1.Prefs) != len(ag2.Prefs) {
		return false
	}

	for i := range ag1.Prefs {
		if ag1.Prefs[i] != ag1.Prefs[i] {
			return false
		}
	}

	return true
}

type AgentI interface {
	Equal(ag AgentI) bool
	DeepEqual(ag AgentI) bool
	Clone() AgentI
	String() string
	Prefers(a Alternative, b Alternative)
}

//Renvoie le rang d'une alternative dans une liste d'alternative
func rank(alt Alternative, prefs []Alternative) int {
	for i := 0; i < len(prefs); i++ {
		if prefs[i] == alt {
			return i
		}
	}
	return -1
}

//Renvoie un tableau car il y a la notion des ex aequos
func MaxCount(count Count) (bestAlts []Alternative) {
	var tmpMax int
	var nbEquals int
	for pref, nb := range count {
		if nb > tmpMax {
			nbEquals = 0
			bestAlts[nbEquals] = pref
		}
		if nb == tmpMax {
			bestAlts[nbEquals] = pref
			nbEquals++
		}
	}
	return bestAlts
}

func checkProfile(prefs Profile) error {
	if prefs == nil {
		return errors.New("This profile doesnt exists")
	}
	if len(prefs) == 0 {
		return errors.New("This profile is empty")
	}
	return nil
}
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	if checkProfile(prefs) != nil {
		return checkProfile(prefs)
	}

	return nil
}
