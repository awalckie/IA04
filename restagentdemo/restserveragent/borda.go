package restserveragent

import (
	rad "gitlab.utc.fr/lagruesy/restagentdemo"
)

func BordaSWF(p rad.Profile) (count rad.Count, err error) {
	for _, v := range p {
		for i := 0; i < len(v); i++ {
			count[v[i]] += len(v) - i
		}
	}
	return count, nil
}

func BordaSCF(p rad.Profile) (bestAlts []rad.Alternative, err error) {
	count, _ := BordaSWF(p)
	return rad.MaxCount(count), nil
}
