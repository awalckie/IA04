package restserveragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
)

type RestServerAgent struct {
	sync.Mutex
	id       string
	reqCount int
	addr     string
	profile  rad.Profile
	hasVoted map[rad.AgentID]bool
}

func NewRestServerAgent(addr string) *RestServerAgent {
	hasVoted := make(map[rad.AgentID]bool)
	profile := [][]rad.Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}
	for i := 0; i < 5; i++ {
		hasVoted[rad.AgentID(i)] = false

	}
	return &RestServerAgent{id: addr, addr: addr, profile: profile, hasVoted: hasVoted}
}

// Test de la méthode
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (*RestServerAgent) decodeRequest(r *http.Request) (req rad.Request, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*RestServerAgent) decodeRequest2(r *http.Request) (req rad.Request2, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

/*
func (rsa *RestServerAgent) doCalc(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decodeRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête
	var resp rad.Response

	switch req.Operator {
	case "*":
		resp.Result = req.Args[0] * req.Args[1]
	case "+":
		resp.Result = req.Args[0] + req.Args[1]
	case "-":
		resp.Result = req.Args[0] - req.Args[1]
	default:
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unkonwn command '%s'", req.Operator)
		w.Write([]byte(msg))
		return
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}
*/
func (rsa *RestServerAgent) doBordaVote(w http.ResponseWriter, r *http.Request) {
	//Bloque le vote une fois fini
	fmt.Println("checks1")
	if isFinished(rsa.hasVoted) {
		w.WriteHeader(http.StatusConflict)
		fmt.Fprint(w, "Le vote est fini")
		msg := fmt.Sprintf("Le vote est fini")
		w.Write([]byte(msg))
		return
	}
	fmt.Println("checks2")
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decodeRequest2(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	if rsa.hasVoted[req.ID] {
		w.WriteHeader(http.StatusConflict)
		fmt.Fprint(w, "Cet identifiant a déja voté")
		msg := fmt.Sprintf("Cet identifiant a déja voté")
		w.Write([]byte(msg))
		return
	} else {
		//On ajoute les préférences dans le profile du serveur
		rsa.profile[req.ID] = req.Prefs
		rsa.hasVoted[req.ID] = true
		/*	msg := fmt.Sprintf("Le vote ", req.Prefs, " de", req.ID, "a été pris en compte")
			w.Write([]byte(msg))*/
	}

	// traitement de la requête
	var resp rad.Response2
	//On ajoute les préférences dans le profile du serveur
	resp.Result = "2"
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)

	if isFinished(rsa.hasVoted) {
		result, _ := BordaSCF(rsa.profile)
		for r := range result {
			fmt.Println(r)
		}
	}
}

func (rsa *RestServerAgent) doBordaResult(w http.ResponseWriter, r *http.Request) {

	if !isFinished(rsa.hasVoted) {
		w.WriteHeader(http.StatusConflict)
		fmt.Fprint(w, "Le vote n'est pas fini")
		msg := fmt.Sprintf("Le vote n'est pas fini")
		w.Write([]byte(msg))
		return
	}
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decodeRequest2(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête
	var resp rad.Response2

	alts, err := BordaSCF(rsa.profile)

	if isArgValid(req) {
		for _, v := range alts {
			resp.Result += string(v)
			fmt.Println(v)
		}
	} else {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unkonwn command '%s'", req.ID)
		w.Write([]byte(msg))
		return
	}

	/* If isValid Type
	   	w.WriteHeader(http.StatusNotImplemented)
	   	msg := fmt.Sprintf("Unkonwn command '%s'", req.Operator)
	   	w.Write([]byte(msg))
	   	return
	   }
	   /**/
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)

}

func (rsa *RestServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()

	mux.HandleFunc("/borda", rsa.doBordaVote)
	mux.HandleFunc("/result", rsa.doBordaResult)
	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}

func isFinished(hasVoted map[rad.AgentID]bool) bool {
	for _, v := range hasVoted {
		if v == false {
			return false
		}
	}
	return true
}

func isArgValid(r rad.Request2) bool {
	return true
}
