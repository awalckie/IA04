package restclientagent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
)

type RestClientAgent struct {
	id        string
	url       string
	id_votant rad.AgentID
	Prefs     []rad.Alternative
}

func NewRestClientAgent(id string, url string, id_votant rad.AgentID, Prefs []rad.Alternative) *RestClientAgent {
	return &RestClientAgent{id, url, id_votant, Prefs}
}

func (rca *RestClientAgent) treatResponse(r *http.Response) int {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.Response
	json.Unmarshal(buf.Bytes(), &resp)

	return resp.Result
}

/*func (rca *RestClientAgent) doRequest() (res int, err error) {
    req := rad.Request{
        Operator: rca.operator,
        Args:     [2]int{rca.arg1, rca.arg2},
    }

    // sérialisation de la requête
    url := rca.url + "/borda"
    data, _ := json.Marshal(req)

    // envoi de la requête
    resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
    fmt.Println(url, "application/json", bytes.NewBuffer(data))
    // traitement de la réponse
    if err != nil {
        return
    }
    if resp.StatusCode != http.StatusOK {
        err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
        return
    }
    res = rca.treatResponse(resp)

    return
}*/

func (rca *RestClientAgent) doRequest() (res int, err error) {
	req := rad.Request{
		ID_votant: rca.id_votant,
		Prefs:     rca.Prefs,
	}

	// sérialisation de la requête
	url := rca.url + "/borda"
	data, _ := json.Marshal(req)

	// envoi de la requête
	fmt.Println("check")
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	fmt.Println(url, "application/json", bytes.NewBuffer(data))
	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	res = rca.treatResponse(resp)

	return
}

func (rca *RestClientAgent) Start() {
	log.Printf("démarrage de %s", rca.id)
	res, err := rca.doRequest()

	if err != nil {
		log.Fatal(rca.id, "error:", err.Error())
	} else {
		log.Printf("[%s] le votant : %d %d\n", rca.id, rca.id_votant, res)
	}
}
