package main

import (
	"fmt"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
	"gitlab.utc.fr/lagruesy/restagentdemo/restclientagent"
)

func main() {
	Prefs := []rad.Alternative{1, 2, 3}
	ag := restclientagent.NewRestClientAgent("id1", "http://localhost:8000", 1, Prefs)
	ag.Start()
	fmt.Scanln()
}
